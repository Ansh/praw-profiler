
from nltk.corpus import brown
from nltk.probability import FreqDist

words = FreqDist()

for sentence in brown.sents():
    for word in sentence:
        words[word.lower()] += 1

with open('stop_words_i_EN', mode='w') as wordlist:
    wordlist.writelines(word + '\r\n' for word, freq in words.most_common(500) if word.isalpha())