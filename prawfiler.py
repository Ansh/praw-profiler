import re
import argparse
import configparser
import datetime

import praw
import prawcore

from collections import defaultdict
from nltk.probability import FreqDist


def display_submission(submission):
    print(submission.title)
    print(submission.score)
    print(submission.id)
    print(submission.url)
    print(submission.author)
    print(submission.author.link_karma)
    print(submission.author.comment_karma)
    print(MESSAGE_DELIMITER)


def find_iama_instances(body):
    """ Return strings 'I am a (result), ...' """
    m = re.findall(r"\bi ?'?a?m a ([\w\s'\"-]+)\b", body, re.IGNORECASE)
    return m


def find_i_instances(body):
    """ Return strings '(I ...).' """
    m = re.findall(r"(\bim? [\w\s'\"(),/\\+-]+)\b", body, re.IGNORECASE)
    return m


def find_possession(body):
    """ Find instances which have phrases like I HAVE (noun) or MY (noun)"""
    result = re.findall(r"\bmy (?P<target>\w+)\b", body, re.IGNORECASE)
    """
    matches = re.findall(r"\b(?:i have|ive|i've) ( a| an| the)?(?P<target>[a-z0-9_ ]+)\b", body, re.IGNORECASE)
    for match in matches:
        m = re.search(r"\b(but|so|now|and|or|once|myself|after|before|since|,).*\b", match or '', re.IGNORECASE)
        if m:
            match = match[:m.start()]
        result.append(match)
    print(result)
    """
    return result


def find_family_members(profile):
    """ Detect family members in possessive context. """
    relatives = [
        'father', 'mother', 'grandfather', 'grandmother', 'granny', 'granddad', 'dad', 'mom', 'daddy', 'mommy',
        'cousin', 'cousins', 'niece', 'aunt', 'uncle', 'mother-in-law', 'father-in-law',
        'child', 'kid', 'son', 'daughter', 'children', 'kids',
        'granddaughter', 'grandson', 'grandma', 'granddad', 'parents', 'papa', 'mama',
        'sibling', 'siblings', 'brother', 'sister', 'bro', 'sis',
        'spouse', 'mate', 'better half',
        'wife', 'husband', 'S.O.', 'significant other',
    ]
    fdist = FreqDist()
    for comment in profile['praw_comments']:
        inst = comment.body
        m = re.findall(r"\b(?:have|got|my) (?P<relative>{})\b".format('|'.join(relatives)), inst, re.IGNORECASE)
        for relative in m:
            fdist[relative] += 1
    profile['relatives'] = [key for key, value in fdist.most_common()]


def find_gender(profile):
    male = [
        'guy', 'dude', 'male', 'man', 'boy',
    ]
    female = [
        'gal', 'dudette', 'female', 'woman', 'girl',
    ]
    fdist = FreqDist()
    for comment in profile['praw_comments']:
        inst = comment.body
        m = re.search(r"\b(?:i'm|i am|im)( a| the| an)? ?[\w\d-]* (?P<male>{})\b".format('|'.join(male)), inst, re.IGNORECASE)
        f = re.search(r"\b(?:i'm|i am|im)( a| the| an)? ?[\w\d-]* (?P<female>{})\b".format('|'.join(female)), inst, re.IGNORECASE)
        if m:
            fdist['male'] +=1
        if f:
            fdist['female'] +=1
    profile['gender'] = fdist.most_common()


def find_location(profile):
    """ Find where the user is located. """
    location_fdist = FreqDist()
    for inst, comment in profile['all_i_instances']:
        m = re.search(r"(im|i'm|am|live) +(in|from) +(a |an |the )?(?P<place>[a-z0-9_ ]+)", inst, re.IGNORECASE)
        if m:
            if m.group('place'):
                match = m.group('place')
                m2 = re.search(r" (but|so|now|and|or|once|myself|after|before|since|,).*", match, re.IGNORECASE)
                if m2:
                    match = match[:m2.start()]
                match = match.strip()
                location_fdist[match] += 1
                if match[0].isupper():  # bonus for capitalized entities
                    location_fdist[match] += 1
    profile['location'] = [ key for key, value in location_fdist.most_common()]


def build_profile(reddit, username):
    user = reddit.redditor(username)
    profile = {
        'praw_user': user,
        'username': username,
        'is_mod': user.is_mod,
        'link_karma': user.link_karma,
        'comment_karma': user.comment_karma,
        'created_utc': user.created_utc,
        'has_verified_email': user.has_verified_email,
        'verified': user.verified,
    }
    return profile


def get_comments(profile, limit, filter_instances=True, tz_analysis=True):
    """ Get comments from reddit. """
    all_instances = []
    all_iama = []
    all_possession = []
    timestamps = []
    fdist_subs = FreqDist()
    popularity_dist = defaultdict(int)
    all_comments = []
    for comment in profile['praw_user'].comments.new(limit=limit):
        all_comments.append(comment)
        subname = comment.subreddit.display_name
        fdist_subs[subname] += 1
        popularity_dist[subname] += comment.score
        all_instances += [(inst, comment) for inst in find_i_instances(comment.body)]
        all_iama += [(inst, comment) for inst in find_iama_instances(comment.body)]
        all_possession += [(inst, comment) for inst in find_possession(comment.body)]
        if tz_analysis:
            timestamps.append(comment.created_utc)
    profile['all_i_instances'] = all_instances
    profile['all_iama'] = all_iama
    profile['all_possession'] = all_possession
    profile['fdist_subs'] = fdist_subs
    profile['popularity_dist'] = popularity_dist
    profile['praw_comments'] = all_comments

    if filter_instances:
        # Filter meaningless sentences out
        checked_instances = []
        with open('stop_words_i_EN') as stopwords_file:
            stopwords = [word.strip() for word in stopwords_file.readlines()]

        for inst in profile['all_i_instances']:
            for word in inst[0].split(' '):
                if word.lower() not in stopwords:
                    checked_instances.append(inst)
                    break
        checked_possession = []
        for word, comment in profile['all_possession']:
            if word.lower() not in stopwords:
                checked_possession.append((word, comment))
        profile['all_possession'] = checked_possession
        profile['i_instances'] = checked_instances
    else:
        profile['i_instances'] = all_instances

    if tz_analysis:
        t_freq = FreqDist()
        for t in timestamps:
            hour = datetime.datetime.utcfromtimestamp(t).hour
            t_freq[hour] += 1
        delta = datetime.datetime.now() - datetime.datetime.utcfromtimestamp(timestamps[-1])
        profile['tz_daysfromfirst'] = delta.days
        delta = datetime.datetime.now() - datetime.datetime.utcfromtimestamp(timestamps[0])
        profile['tz_daysfromlast'] = delta.days
        profile['tz_freq'] = t_freq


def display_console(profile, is_show_detailed=False, is_show_all=False):
    print('Info about ' + profile['username'])
    print(MESSAGE_DELIMITER)
    print('Is mod: {}'.format(profile['is_mod']))
    print('Verified: {}'.format(profile['verified']))
    print('Has verified email: {}'.format(profile['has_verified_email']))
    print('link karma: {}'.format(profile['link_karma']))
    print('comment karma: {}'.format(profile['comment_karma']))
    created_on = datetime.date.fromtimestamp(profile['created_utc'])
    delta = datetime.date.today() - created_on
    print('created on {}.'.format(created_on))
    print('Is {} year(s) old'.format(delta.days // 365))
    print(MESSAGE_DELIMITER)

    if profile.get('tz_freq'):
        print('We have analyzed comments for the last {} days.'.format(profile['tz_daysfromfirst']))
        print('The last comment was left {} days ago.'.format(profile['tz_daysfromlast']))
        print('In average, the user has left {0:.3f} comments per day.'.format(
            len(profile['praw_comments']) / (profile['tz_daysfromfirst'] - profile['tz_daysfromlast'])
        ))
        fdist = profile['tz_freq']
        if is_show_detailed:
            print('Here goes the frequency distribution of comments posting hours (UTC hours above, freq below):')
            fdist.tabulate()

        def find_max_freq_hours(fdist):
            """ Find the period of highest activity. """
            max_freq_hour = fdist.max()
            first_max = [max_freq_hour]
            for hour, freq in fdist.most_common():
                if hour in first_max:
                    continue
                elif hour - 1 in first_max or (hour == 0 and sorted(first_max)[-1] == 23):
                    if freq * 2 > (fdist[sorted(first_max)[-1]]):
                        first_max.append(hour)
                elif hour + 1 in first_max or (hour == 23 and sorted(first_max)[0] == 0):
                    if freq * 2 > (fdist[sorted(first_max)[0]]):
                        first_max.append(hour)
            return sorted(first_max)

        def find_min_freq_hours(fdist):
            """ Find the period of lowest activity. """
            min_freq_hour = sorted(fdist.items(), key=lambda a: a[1])[0][0]
            first_min = [min_freq_hour]
            for hour, freq in reversed(fdist.most_common()):
                if hour in first_min:
                    continue
                elif hour - 1 in first_min or (hour == 0 and sorted(first_min)[-1] == 23):
                    if freq / 2 < (fdist[sorted(first_min)[-1]] + 1):
                        first_min.append(hour)
                elif hour + 1 in first_min or (hour == 23 and sorted(first_min)[0] == 0):
                    if freq / 2 < (fdist[sorted(first_min)[0]] + 1):
                        first_min.append(hour)
            return sorted(first_min)

        max_hours = find_max_freq_hours(fdist)
        min_hours = find_min_freq_hours(fdist)

        print('Highest activity is registered from {} to {} UTC'.format(max_hours[0], max_hours[-1]))
        print('Lowest activity is registered from {} to {} UTC'.format(min_hours[0], min_hours[-1]))

    def print_instances(instances, is_detailed=False):
        """ Help displaying instances information in console, using print command. """
        for inst, comment in instances:
            if is_detailed:
                print("'{}' (from {}'s thread '{}').".format(
                    inst,
                    comment.subreddit_name_prefixed,
                    comment.link_title
                ))
            else:
                print(inst)
        print('')

    if profile.get('gender'):
        print(MESSAGE_DELIMITER)
        print('The user\'s gender is {}'.format(profile['gender']))
    if profile.get('location'):
        print(MESSAGE_DELIMITER)
        print('The user lives(lived)git st in: {}'.format(", ".join(profile['location'])))
    if profile.get('relatives'):
        print(MESSAGE_DELIMITER)
        print('The user might have mentioned their relatives in the following sentences:')
        for relative in profile['relatives']:
            print('"{}"'.format(relative))
    if profile.get('all_possession'):
        print(MESSAGE_DELIMITER)
        print('This user has:')
        print_instances(user_profile['all_possession'], is_show_detailed)
    print(MESSAGE_DELIMITER)
    print('Analyzed {} comments.'.format(len(profile['praw_comments'])))
    print('No of I AM A instances: ' + str(len(profile['all_iama'])))
    print('No of I-instances: ' + str(len(user_profile['i_instances'])))

    if profile['all_iama']:
        print('\r\n"I am a..." instances:')
        print(MESSAGE_DELIMITER)
        print_instances(profile['all_iama'], is_show_detailed)
        print(MESSAGE_DELIMITER)

    if is_show_all:
        print('Sentences about themselves:')
        print_instances(user_profile['i_instances'], is_show_detailed)
        print(MESSAGE_DELIMITER)

    print('Subreddits Commenting Frequency:')
    for name, freq in profile['fdist_subs'].most_common(10):
        print("{} - {}%".format(name, int(profile['fdist_subs'].freq(name)*100)))
    print(MESSAGE_DELIMITER)

    print('===Subreddits Popularity (more than 3 posts)===')
    pop_per_freq = {}  # average popularity per posts
    for name, freq in profile['fdist_subs'].items():
        if freq > 3:
            pop_per_freq[name] = profile['popularity_dist'][name] // freq
    print('Most Popular:')
    for name, freq in sorted(pop_per_freq.items(), key=lambda item: item[1], reverse=True)[:10]:
        print("{} - {}".format(name, freq))
    print('\r\nMost Unpopular:')
    for name, freq in sorted(pop_per_freq.items(), key=lambda item: item[1])[:5]:
        print("{} - {}".format(name, freq))
    print(MESSAGE_DELIMITER)


def connect():
    return praw.Reddit(client_id=config['Reddit']['client_id'],
                       client_secret=config['Reddit']['client_secret'],
                       user_agent=config['Reddit']['user_agent'])

config = configparser.ConfigParser()
config.read('config.ini')

try:
    DEFAULT_REDDITOR_NAME = config['DEFAULT']['REDDITOR_NAME']
except configparser.Error:
    DEFAULT_REDDITOR_NAME = 'spez'
try:
    COMMENTS_LIMIT = int(config['DEFAULT']['COMMENTS_LIMIT'])
except (ValueError, configparser.Error):
    COMMENTS_LIMIT = 300

MESSAGE_DELIMITER = "~~~~~~"


if __name__ == '__main__':
    # Parse commandline arguments
    parser = argparse.ArgumentParser(description='Reddit user profiler.')
    parser.add_argument('-user', type=str, help='Reddit\'s username', default=None)
    parser.add_argument('-limit', type=int,
                        help='Default is {}. A number of processed comments.'.format(COMMENTS_LIMIT))
    parser.add_argument('-all', action='store_true', help='Display all mined info.')
    parser.add_argument('-detailed', action='store_true', help='Provide more details (takes longer to process).')
    args = parser.parse_args()
    reddit = connect()
    try:
        user_profile = build_profile(reddit, args.user or DEFAULT_REDDITOR_NAME)
    except prawcore.exceptions.NotFound:
        print('USER {} NOT FOUND'.format(args.user or DEFAULT_REDDITOR_NAME))
        quit()
    get_comments(user_profile, args.limit or COMMENTS_LIMIT)
    find_location(user_profile)
    find_family_members(user_profile)
    find_gender(user_profile)
    display_console(user_profile, args.detailed, args.all)
